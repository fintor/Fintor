defmodule FintorWeb.PageController do
  use FintorWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
