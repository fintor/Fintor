# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :fintor,
  ecto_repos: [Fintor.Repo]

# Configures the endpoint
config :fintor, FintorWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "j6IUVQu1U4xLybCn0b+pkmc7l5QNLzQOfT7PWh4af1wDmy+HKE5+p31jeIjoxEcE",
  render_errors: [view: FintorWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Fintor.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
