use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :fintor, FintorWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :fintor, Fintor.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("PG_TEST_USER") || "postgres",
  password: System.get_env("PG_TEST_PASSWORD") || "postgres",
  database: System.get_env("PG_TEST_DB") || "fintor_test",
  hostname: System.get_env("PG_TEST_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
